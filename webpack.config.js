var path = require('path')
var webpack = require('webpack')
var autoprefixer = require('autoprefixer');
var precss = require('precss');
// var NpmInstallPlugin = require('npm-install-webpack-plugin');

module.exports = {
	resolve: {
		extensions: ['.js', '.jsx', '.es6']
	},

	devtool: 'cheap-module-eval-source-map',

	entry: [
		'webpack-hot-middleware/client',
		'babel-polyfill',
		'./src/index'
	],

	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js',
		publicPath: '/static/'
	},

	plugins: [
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		// new NpmInstallPlugin()
	],

	module: {
		rules: [
			{
				test: /\.js$/,
				include: path.resolve(__dirname, 'src'),
				loader: 'eslint-loader',
				enforce: 'pre',
			},
			{
				test: /\.js$/,
				include: path.resolve(__dirname, 'src'),
				use: [
					{
						loader: 'react-hot-loader'
					},
					{
						loader: 'babel-loader',
						options: {
							babelrc: false,
							presets: [
								'es2015',
								'stage-0',
								'react'
							],
							plugins: ['transform-runtime']
						}
					}
				]
			},
			{
				test: /\.css$/,
				include: path.resolve(__dirname, 'src'),
				use: [
					'style-loader',
					'css-loader',
					{
						loader: 'postcss-loader',
						options: {
							plugins: [
								autoprefixer,
								precss
							]
						}
					}
				]
			}
		]
	}
}
